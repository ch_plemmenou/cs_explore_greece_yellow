package com.yellow.exploregreece.features.tours.data;

import com.yellow.exploregreece.features.tours.domain.Tour;
import com.yellow.exploregreece.features.tours.domain.TourInteractor;
import com.yellow.exploregreece.rest.RestClient;

import android.content.Context;
import android.os.AsyncTask;

import com.yellow.exploregreece.base.ExploreGreeceDatabase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TourInteractorImpl implements TourInteractor {


    @Override
    public void getTours(final OnToursFinishListener listener, final Context ctx, final String tourpackage_id) {
        //public void getTours(final OnToursFinishListener listener, String tourPackId, final Context ctx)
        // listener.onSuccess(insertMockTours());

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                final TourDao dao = ExploreGreeceDatabase.getDatabase(ctx).toursDao();
                List<Tour> toursFromDb = dao.getAllToursForPackage(tourpackage_id);
                ArrayList<Tour> arrayListFromDb = new ArrayList<>();
                arrayListFromDb.addAll(toursFromDb);

                if(toursFromDb.isEmpty()) {
//                    Call<ArrayList<Tour>> call = RestClient.call().fetchTours();
                    Call<ArrayList<Tour>> call = RestClient.call().fetchTours(tourpackage_id);
                    call.enqueue(new Callback<ArrayList<Tour>>() {

                        @Override
                        public void onResponse(Call<ArrayList<Tour>> call, Response<ArrayList<Tour>> response) {

                           final ArrayList<Tour> tours = response.body();
                           for (Tour tour : tours){
                               tour.setTourpackage_id(tourpackage_id);
                           }
                            AsyncTask.execute(new Runnable() {
                                @Override
                                public void run() {
                                    dao.removeTours();
                                    dao.insertTours(tours);
                                }
                            });
                            listener.onSuccess(tours);

                        }

                        @Override
                        public void onFailure(Call<ArrayList<Tour>> call, Throwable t) {
                            listener.onError();
                        }

                    });
                }else
                    listener.onSuccess(arrayListFromDb);

            }
        });


    }

}


