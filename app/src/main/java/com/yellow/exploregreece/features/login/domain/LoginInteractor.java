package com.yellow.exploregreece.features.login.domain;

import android.content.Context;

public interface LoginInteractor {

    void login(OnLoginFinishListener listener, Context ctx, String username, String password);

    interface OnLoginFinishListener {

        void onSuccess();

        void onError(String message);

    }

}
