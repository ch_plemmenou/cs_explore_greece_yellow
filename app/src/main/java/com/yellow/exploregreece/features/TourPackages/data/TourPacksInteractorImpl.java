package com.yellow.exploregreece.features.TourPackages.data;


import android.content.Context;
import android.os.AsyncTask;

import com.yellow.exploregreece.base.ExploreGreeceDatabase;
import com.yellow.exploregreece.features.TourPackages.domain.TourPackageDomain;
import com.yellow.exploregreece.features.TourPackages.domain.TourPacksInteractor;
import com.yellow.exploregreece.rest.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TourPacksInteractorImpl implements TourPacksInteractor {

    @Override
    public void getTourPacks(final OnTourPacksFinishListener listener, final Context ctx, final boolean refresh) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final TourPackagesDao tourPackagesDao = ExploreGreeceDatabase.getDatabase(ctx).tourPackagesDao();
                List<TourPackageDomain> tourPackagesFromDb = tourPackagesDao.getAllTourPackages();
                ArrayList<TourPackageDomain> arrayListFromDb = new ArrayList<>();
                arrayListFromDb.addAll(tourPackagesFromDb);
                if (tourPackagesFromDb.isEmpty() || refresh) {
                    Call<ArrayList<TourPackageDomain>> call = RestClient.call().fetchTourPackages();
                    call.enqueue(new Callback<ArrayList<TourPackageDomain>>() {

                        @Override
                        public void onResponse(Call<ArrayList<TourPackageDomain>> call, final Response<ArrayList<TourPackageDomain>> response) {
                            try {
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        tourPackagesDao.deleteTourPackages();
                                        tourPackagesDao.insertTourPackages(response.body());
                                        listener.onSuccess(response.body());
                                    }
                                });

                            } catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArrayList<TourPackageDomain>> call, Throwable t) {
                            Timber.e("Failed to fetch TourPackages from the server");
                            listener.onError();
                        }

                    });

                }
                else
                    listener.onSuccess(arrayListFromDb);
                }

            });
        }




    @Override
    public void getFilteredTourPacks(final OnTourPacksFinishListener listener, final String filterString) {
        Call<ArrayList<TourPackageDomain>> call = RestClient.call().fetchTourPackages();
        call.enqueue(new Callback<ArrayList<TourPackageDomain>>() {
            @Override
            public void onResponse( Call<ArrayList<TourPackageDomain>> call, Response<ArrayList<TourPackageDomain>> response) {

                ArrayList<TourPackageDomain> filteredTourPacks = new ArrayList<>();
                ArrayList<TourPackageDomain> tourpacks = response.body();
                for (int i = 0; i < tourpacks.size(); i++) {
                    if (tourpacks.get(i).getName().startsWith(filterString))
                        filteredTourPacks.add(tourpacks.get(i));
                }
                listener.onSuccess(filteredTourPacks);

            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<TourPackageDomain>> call, Throwable t) {

            }
        });


    }
}

 /*private ArrayList<TourPackageDomain> getMockedTourPacks() {
        ArrayList<TourPackageDomain> tourPackages = new ArrayList<>();
        tourPackages.add(new TourPackageDomain("1", "Crete", 5 ));
        tourPackages.add(new TourPackageDomain("2","Peloponnese", 3));
        tourPackages.add(new TourPackageDomain("3","Macedonia", 3));
        tourPackages.add(new TourPackageDomain("4","Thesally", 2));
        tourPackages.add(new TourPackageDomain("5","Thrase", 4));
        tourPackages.add(new TourPackageDomain("6","Aegean", 3));
        tourPackages.add(new TourPackageDomain("7","Ionian", 2));
        tourPackages.add(new TourPackageDomain("8", "Sterea Hellas", 4));

        return tourPackages;
    }*/




