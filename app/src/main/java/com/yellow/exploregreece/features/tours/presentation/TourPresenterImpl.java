package com.yellow.exploregreece.features.tours.presentation;

import android.content.Context;

import com.yellow.exploregreece.features.tours.data.TourInteractorImpl;
import com.yellow.exploregreece.features.tours.domain.Tour;
import com.yellow.exploregreece.features.tours.domain.TourInteractor;
import com.yellow.exploregreece.features.tours.domain.TourPresenter;
import com.yellow.exploregreece.features.tours.domain.TourView;

import java.util.ArrayList;

public class TourPresenterImpl implements TourPresenter, TourInteractor.OnToursFinishListener {

      private TourView tourView;
    private TourInteractor interactor;

    public TourPresenterImpl(TourView tourView) {
        this.tourView = tourView;
        this.interactor = new TourInteractorImpl();
    }

    @Override
    public void getTours(Context ctx, String tourpackage_id, boolean refresh) {
        //public void getTours(String tourPackId) {
        //interactor.getTours(this, tourPackId);
        interactor.getTours(this, ctx, tourpackage_id);
    }


    @Override
    public void onSuccess(ArrayList<Tour> tours) {
        ArrayList<TourUI> toursUI = new ArrayList<>();
        for (Tour tourDomain : tours) {
            TourUI tourUI = new TourUI(tourDomain.getTitle(),
                    tourDomain.getDescription());

            toursUI.add(tourUI);
        }
        tourView.showTours(toursUI);
    }

    @Override
    public void onError() {

    }
}