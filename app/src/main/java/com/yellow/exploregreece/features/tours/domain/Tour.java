package com.yellow.exploregreece.features.tours.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "tour")
public class Tour {

    // id : String, name: String description : String, descTextSize : int
    @PrimaryKey
    @NonNull
    String id;
    String title;
    String description;
    int price;
    String duration;
    String bullets;
    String keywords;
    String tourpackage_id;


    public Tour(@NonNull String id, String title, String description, int price, String duration, String bullets, String keywords) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.bullets = bullets;
        this.keywords = keywords;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBullets() {
        return bullets;
    }

    public void setBullets(String bullets) {
        this.bullets = bullets;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getTourpackage_id() {
        return tourpackage_id;
    }

    public void setTourpackage_id(String tourpackage_id) {
        this.tourpackage_id = tourpackage_id;
    }
}

