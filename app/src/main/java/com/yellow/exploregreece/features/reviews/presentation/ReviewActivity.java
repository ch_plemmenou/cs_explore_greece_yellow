package com.yellow.exploregreece.features.reviews.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.yellow.exploregreece.R;

public class ReviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.review_activity, new ReviewFragment())
                .commit();

    }
}
