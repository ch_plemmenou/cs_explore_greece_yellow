package com.yellow.exploregreece.features.login.domain;

public interface LoginView {

    void isLoggedIn(boolean loggedIn,String message);

}
