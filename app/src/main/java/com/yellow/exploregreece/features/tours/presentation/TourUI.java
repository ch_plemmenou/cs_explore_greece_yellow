package com.yellow.exploregreece.features.tours.presentation;

public class TourUI {

    String tourId;
    String title;
    String description;

    public TourUI(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TourUI(String tourId, String title, String description) {

        this.tourId = tourId;
        this.title = title;
        this.description = description;
    }
}
