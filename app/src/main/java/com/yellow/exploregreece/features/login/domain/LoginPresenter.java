package com.yellow.exploregreece.features.login.domain;

import android.content.Context;

public interface LoginPresenter {

    void checkCredentials(String username, String password, Context ctx);

}
