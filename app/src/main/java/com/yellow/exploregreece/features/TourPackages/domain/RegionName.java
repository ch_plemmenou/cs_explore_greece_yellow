package com.yellow.exploregreece.features.TourPackages.domain;

public enum RegionName {

    CRETE("Crete"),
    PELOPONNESE("Peloponnese"),
    MACEDONIA("Macedonia"),
    THESSALY("Thessaly"),
    THRACE("Thrace"),
    AEAGEAN("Aeagean"),
    IONIAN("Ionian"),
    STEREAHELLAS("Sterea Hellas");


    private final String string;

    RegionName(String string){
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
