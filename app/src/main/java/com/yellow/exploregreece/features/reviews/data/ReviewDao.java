package com.yellow.exploregreece.features.reviews.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.yellow.exploregreece.features.reviews.domain.Review;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ReviewDao {

   @Insert
   void insertReviews(ArrayList<Review> reviews);

   @Query("SELECT * FROM review")
   List<Review> getAllReviews();

   @Query("SELECT * FROM review where tourPackageId=:tourpackage_id")
   List<Review> getAllReviewsForPackage(String tourpackage_id);

   @Query("DELETE FROM review")
   void deleteReviews();

}
