package com.yellow.exploregreece.features.tours.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.yellow.exploregreece.features.tours.domain.Tour;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface TourDao {

    @Insert
    void insertTours(ArrayList<Tour> tours);

    @Query("SELECT * FROM tour")
    List<Tour> getAllTours();

    @Query("SELECT * FROM tour where tourpackage_id =:tourpackage_id")
    List<Tour> getAllToursForPackage(String tourpackage_id);

    @Query("DELETE FROM tour")
    void removeTours();
}


