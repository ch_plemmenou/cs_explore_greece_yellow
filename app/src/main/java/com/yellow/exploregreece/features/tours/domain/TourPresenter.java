package com.yellow.exploregreece.features.tours.domain;

import android.content.Context;

public interface TourPresenter {

    void getTours(Context ctx, String tourPackageID, boolean refresh);

}
