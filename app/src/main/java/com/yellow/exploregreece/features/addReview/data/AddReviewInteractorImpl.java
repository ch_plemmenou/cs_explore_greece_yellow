package com.yellow.exploregreece.features.addReview.data;

import android.content.Context;
import android.os.AsyncTask;

import com.yellow.exploregreece.base.ExploreGreeceDatabase;
import com.yellow.exploregreece.features.addReview.data.ReviewDomain;
import com.yellow.exploregreece.features.addReview.domain.AddReviewInteractor;
import com.yellow.exploregreece.features.login.data.UserDao;
import com.yellow.exploregreece.features.login.domain.UserDomain;
import com.yellow.exploregreece.rest.RestClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReviewInteractorImpl implements AddReviewInteractor {


    @Override
    public void sendReview(final OnSendReviewListener listener, final Context ctx, final int score, final String comment, final String tourpackageID) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                UserDao userDao = ExploreGreeceDatabase.getDatabase(ctx).userDao();
                ArrayList<UserDomain> usersList = (ArrayList<UserDomain>)userDao.getUsers();
                UserDomain loggedInUser = usersList.get(0);

                ReviewDomain newReview = new ReviewDomain(score, comment, loggedInUser.getUsername());

                Call<Void> call = RestClient.call().sendReview(tourpackageID, newReview);

                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.code() == 200 || response.code() == 201) {
                            listener.onSuccess();
                        } else {
                            listener.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        listener.onError();
                    }
                });
            }
        });
    }

}
