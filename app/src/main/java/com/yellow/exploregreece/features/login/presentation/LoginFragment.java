package com.yellow.exploregreece.features.login.presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.presentation.TourPacksActivity;
import com.yellow.exploregreece.features.login.domain.LoginPresenter;
import com.yellow.exploregreece.features.login.domain.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements LoginView {


    @BindView(R.id.username_edittext)
    EditText mUsernameEditText;

    @BindView(R.id.password_edittext)
    EditText mPasswordEditText;

    @BindView(R.id.login_btn)
    Button mLoginButton;

    LoginPresenter loginPresenter;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        ButterKnife.bind(this, view);
        loginPresenter = new LoginPresenterImpl(this);
//        mUsernameEditText.setText("teamYellow");
//        mPasswordEditText.setText("pAssyyelL0w");


        // Inflate the layout for this fragment
        return view;
    }

    @OnClick(R.id.login_btn)
    public void login(View view) {

        String usrname = mUsernameEditText.getText().toString();
        String psw = mPasswordEditText.getText().toString();

        loginPresenter.checkCredentials(usrname,psw,getActivity());

    }


    @Override
    public void isLoggedIn(boolean loggedIn, String errorMessage) {
        if (loggedIn){
            Intent intent = new Intent(getActivity(),TourPacksActivity.class);
            startActivity(intent);
            getActivity().finish();
        }else{
            Toast.makeText(getActivity(),getResources().getString(R.string.no_login_message),Toast.LENGTH_LONG).show();
        }

    }
}
