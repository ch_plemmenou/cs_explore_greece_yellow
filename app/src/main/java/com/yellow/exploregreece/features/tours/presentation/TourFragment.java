package com.yellow.exploregreece.features.tours.presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.presentation.TourPackageUI;
import com.yellow.exploregreece.features.addReview.presenter.AddReviewActivity;
import com.yellow.exploregreece.features.reviews.presentation.ReviewActivity;
import com.yellow.exploregreece.features.tours.domain.OnTourClickListener;
import com.yellow.exploregreece.features.tours.domain.TourPresenter;
import com.yellow.exploregreece.features.tours.domain.TourView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TourFragment extends Fragment implements TourView {


    @BindView(R.id.tours_recycler_view)
    RecyclerView toursRv;

    @BindView(R.id.see_all_ratings_button)
    FloatingActionButton mySeeAllReviewsFloatingButton;

    @BindView(R.id.top_screen_tourpackage_name)
    TextView myTourPackageName;

    @BindView(R.id.top_screen_tourpackage_rating)
    RatingBar myTourPackageRating;

    @BindView(R.id.floating_add_rating_button)
    FloatingActionButton myAddReviewFloatingButton;


    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;

    @BindView(R.id.tour_root)
    SwipeRefreshLayout mTourRoot;

    private TourPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        TourFragment myFragment = new TourFragment();
        Intent intent = getActivity().getIntent();
        final TourPackageUI tourpackage = intent.getExtras().getParcelable("tourpackage");



        View v = inflater.inflate(R.layout.fragment_tour, container, false);
        ButterKnife.bind(this, v);
        mTourRoot.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        presenter.getTours(getActivity(),tourpackage.getTourpackId(),true);
                    }
                });


        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(myToolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);

        myTourPackageName.setText(tourpackage.getName());
        myTourPackageRating.setRating(Float.valueOf(String.valueOf(tourpackage.getRating())));


        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
//                Intent intent = new Intent(getActivity(), TourPacksActivity.class);
//                getActivity().startActivity(intent);
            }
        });
        myAddReviewFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AddReviewActivity.class);
                intent.putExtra("tourpackage_id",tourpackage.getTourpackId());
                getActivity().startActivity(intent);

            }
        });
        mySeeAllReviewsFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ReviewActivity.class);
                intent.putExtra("tourpackage",tourpackage);
                getActivity().startActivity(intent);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        toursRv.setLayoutManager(layoutManager);
        presenter = new TourPresenterImpl(this);
        presenter.getTours(getActivity(),tourpackage.getTourpackId(),false);
        return v;
    }


    @Override
    public void showTours(ArrayList<TourUI> tours) {
        mTourRoot.setRefreshing(false);

        TourRvAdapter toursRvAdapter = new TourRvAdapter(tours, new OnTourClickListener() {
            @Override
            public void onTourClicked(TourUI tour) {
                Toast.makeText(getActivity(), tour.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });
        toursRv.setAdapter(toursRvAdapter);

    }
}
