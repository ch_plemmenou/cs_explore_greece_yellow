package com.yellow.exploregreece.features.TourPackages.domain;

import android.content.Context;

public interface TourPacksPresenter {

    void getTourPacks(Context ctx, boolean refresh);

    void getFilteredTourPacks(String filter);
}
