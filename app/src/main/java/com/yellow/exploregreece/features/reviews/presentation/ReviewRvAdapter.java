package com.yellow.exploregreece.features.reviews.presentation;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.yellow.exploregreece.R;

import java.util.ArrayList;

public class ReviewRvAdapter extends RecyclerView.Adapter<ReviewRvAdapter.ReviewsViewHolder> {


    private ArrayList<ReviewUI> reviews;
    private Context context;



    public ReviewRvAdapter(ArrayList<ReviewUI> reviews, Context context) {
        this.reviews = reviews;
        this.context = context;
    }


    public static class ReviewsViewHolder extends RecyclerView.ViewHolder {


        TextView mReviewDescription;
        TextView mReviewUsername;
        RatingBar mReviewRatingBar;
        LinearLayout mReviewItemRoot;


        public ReviewsViewHolder(View v) {
            super(v);
            mReviewDescription = v.findViewById(R.id.review_description);
            mReviewUsername = v.findViewById(R.id.review_username);
            mReviewRatingBar = v.findViewById(R.id.review_rating_bar);
            mReviewItemRoot = v.findViewById(R.id.review_item_root);
        }
    }

    @NonNull
    @Override
    public ReviewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_review_item, viewGroup, false);
        ReviewsViewHolder vh = new ReviewsViewHolder(v);

        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull ReviewsViewHolder viewHolder, int position) {
        final ReviewUI review = reviews.get(position);
        viewHolder.mReviewDescription.setText(review.getDescription());
        viewHolder.mReviewUsername.setText(review.getName());
        viewHolder.mReviewRatingBar.setRating(review.getRating());
        DrawableCompat.setTint(viewHolder.mReviewRatingBar.getProgressDrawable(), context.getResources().getColor(R.color.Orange));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            viewHolder.mReviewDescription.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

}



















