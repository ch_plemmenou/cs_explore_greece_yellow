package com.yellow.exploregreece.features.TourPackages.domain;

import com.yellow.exploregreece.features.TourPackages.presentation.TourPackageUI;

public interface OnTourPacksClickListener {

    void onTourPacksClicked(TourPackageUI tourPackage);
}
