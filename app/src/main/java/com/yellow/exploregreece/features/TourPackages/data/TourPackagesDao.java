package com.yellow.exploregreece.features.TourPackages.data;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.yellow.exploregreece.features.TourPackages.domain.TourPackageDomain;

import java.util.ArrayList;
import java.util.List;


@Dao
public interface TourPackagesDao {

    @Insert
    void insertTourPackages(ArrayList<TourPackageDomain> tourpackages);

    @Query("SELECT * FROM tourPackage")
    List<TourPackageDomain> getAllTourPackages();

    @Query("DELETE FROM tourPackage")
    void deleteTourPackages();
}
