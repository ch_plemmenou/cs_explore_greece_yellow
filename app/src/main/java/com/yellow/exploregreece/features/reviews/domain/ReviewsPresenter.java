package com.yellow.exploregreece.features.reviews.domain;

import android.content.Context;

public interface ReviewsPresenter {

    void getReviews(Context ctx, String tourpackage_id,boolean refresh);

}
