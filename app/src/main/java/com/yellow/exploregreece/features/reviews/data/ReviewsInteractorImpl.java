package com.yellow.exploregreece.features.reviews.data;

import android.content.Context;
import android.os.AsyncTask;

import com.yellow.exploregreece.base.ExploreGreeceDatabase;
import com.yellow.exploregreece.features.reviews.domain.Review;
import com.yellow.exploregreece.features.reviews.domain.ReviewInteractor;
import com.yellow.exploregreece.rest.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewsInteractorImpl implements ReviewInteractor {

    @Override
    public void getReviews(final OnReviewsFinishListener listener, final Context ctx, final String tourpackage_id, final boolean refresh) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final ReviewDao dao = (ReviewDao) ExploreGreeceDatabase.getDatabase(ctx).reviewsDao();
                List<Review> reviewsFromDb = dao.getAllReviewsForPackage(tourpackage_id);
                ArrayList<Review> arrayListFromDb = new ArrayList<>();
                arrayListFromDb.addAll(reviewsFromDb);

                if (reviewsFromDb.isEmpty() || refresh) {
                    Call<ArrayList<Review>> call = RestClient.call().fetchReviews(tourpackage_id);
                    call.enqueue(new Callback<ArrayList<Review>>() {

                        @Override
                        public void onResponse(Call<ArrayList<Review>> call, Response<ArrayList<Review>> response) {

                            final ArrayList<Review> reviews = response.body();
                            AsyncTask.execute(new Runnable() {
                                @Override
                                public void run() {
                                    for(Review review : reviews) {
                                        review.setTourPackageId(tourpackage_id);
                                    }
                                    dao.deleteReviews();
                                    dao.insertReviews(reviews);
                                }
                            });
                            listener.onSuccess(reviews);


                        }

                        @Override
                        public void onFailure(Call<ArrayList<Review>> call, Throwable t) {
                            listener.onError();
                        }

                    });
                } else
                    listener.onSuccess(arrayListFromDb);
            }

        });


    }

}
