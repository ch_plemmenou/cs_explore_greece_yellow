package com.yellow.exploregreece.features.TourPackages.presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.domain.OnTourPacksClickListener;
import com.yellow.exploregreece.features.TourPackages.domain.TourPacksPresenter;
import com.yellow.exploregreece.features.TourPackages.domain.TourPacksView;
import com.yellow.exploregreece.features.tours.presentation.TourActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class TourPacksFragment extends Fragment implements TourPacksView {

    @BindView(R.id.filter_edit_text)
    EditText mFilterEditText;

    @BindView(R.id.filter_button)
    ImageButton mFilterButton;

    @BindView(R.id.tourpack_rv)
    RecyclerView tourpacksRv;

    @BindView(R.id.tourpackages_roots)
    SwipeRefreshLayout mTourPacksRoot;

    TourPacksPresenter presenter;

    public TourPacksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tour_packs, container, false);
        ButterKnife.bind(this, v);

        mTourPacksRoot.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getTourPacks(getActivity(),true);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        tourpacksRv.setLayoutManager(layoutManager);
        presenter = new TourPacksPresenterImpl(this);
        presenter.getTourPacks(getActivity(),false);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);


        return v;
    }

    @Override
    public void showTourPacks (final ArrayList<TourPackageUI> tourPackages) {

        mTourPacksRoot.setRefreshing(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        getActivity().runOnUiThread(new Runnable() {
            TourPacksRvAdapter tourPacksRvAdapter = new TourPacksRvAdapter(tourPackages, new OnTourPacksClickListener() {
                @Override
                public void onTourPacksClicked(TourPackageUI tourPackage) {
                    Intent intent = new Intent(getActivity(), TourActivity.class);
                    intent.putExtra("tourpackage", tourPackage);
                    getActivity().startActivity(intent);
                }
            },getActivity());

            @Override
            public void run() {
                tourpacksRv.setAdapter(tourPacksRvAdapter);
            }
        });

    }

    @Override
    public void showGeneralError() {
        mTourPacksRoot.setRefreshing(false);
        Toast.makeText(getContext(), "oops!! Something went wrong! Please try again later" , Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.filter_button)
    public void filterTourPacks(View view) {
        String filterText = mFilterEditText.getText().toString();
        presenter.getFilteredTourPacks(filterText);
    }



}
