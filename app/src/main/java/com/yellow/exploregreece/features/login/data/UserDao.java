package com.yellow.exploregreece.features.login.data;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.yellow.exploregreece.features.login.domain.UserDomain;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insertUser(UserDomain user);

    @Query("SELECT * FROM user where password=:password and username=:username")
    UserDomain getLoggedInUser(String password, String username);

    @Query("SELECT * FROM user")
    List<UserDomain> getUsers();

}
