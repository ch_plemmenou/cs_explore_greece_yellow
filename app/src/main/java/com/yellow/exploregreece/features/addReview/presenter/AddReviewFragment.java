package com.yellow.exploregreece.features.addReview.presenter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.addReview.domain.AddReviewPresenter;
import com.yellow.exploregreece.features.addReview.domain.AddReviewView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddReviewFragment extends Fragment implements AddReviewView {

    @BindView(R.id.add_rating_bar)
    RatingBar ratingBar;

    @BindView(R.id.review_edittext)
    EditText reviewEdittext;

    AddReviewPresenter addReviewPresenter;
    private String tourpackageID;

    public AddReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_review, container, false);

        tourpackageID = getActivity().getIntent().getStringExtra("tourpackage_id");

        ButterKnife.bind(this, v);

        addReviewPresenter = new AddReviewPresenterImpl(this);

        return v;
    }

    @Override
    public void reviewSent(boolean isSuccess) {
        if (isSuccess){
            Toast.makeText(getActivity(),R.string.succesfull_addreview,Toast.LENGTH_LONG).show();
            getActivity().finish();
        }else{
            Toast.makeText(getActivity(),R.string.error_addreview,Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.add_review_button)
    public void addReview(View view) {
        int score = (int) ratingBar.getRating();
        String comment = reviewEdittext.getText().toString();

        addReviewPresenter.sendReview(tourpackageID, score,comment, getActivity());

    }
}
