package com.yellow.exploregreece.features.reviews.presentation;

import android.content.Context;

import com.yellow.exploregreece.features.reviews.domain.Review;
import com.yellow.exploregreece.features.reviews.domain.ReviewInteractor;
import com.yellow.exploregreece.features.reviews.domain.ReviewsPresenter;
import com.yellow.exploregreece.features.reviews.domain.ReviewsView;
import com.yellow.exploregreece.features.reviews.data.ReviewsInteractorImpl;

import java.util.ArrayList;

public class ReviewsPresenterImpl implements ReviewsPresenter, ReviewInteractor.OnReviewsFinishListener {


    private ReviewsView reviewsView;
    private ReviewInteractor interactor;


    public ReviewsPresenterImpl(ReviewsView reviewsView) {
        this.reviewsView = reviewsView;
        this.interactor = new ReviewsInteractorImpl();
    }


    @Override
    public void getReviews(Context ctx, String tourpackage_id, boolean refresh) {
        interactor.getReviews(this, ctx,tourpackage_id, refresh);

    }

       @Override
        public void onSuccess(ArrayList<Review> reviews) {
            ArrayList<ReviewUI> reviewsUI = new ArrayList<>();
            for (Review review : reviews) {
                ReviewUI reviewUI = new ReviewUI(review.getUsername(),
                        review.getComment(),review.getScore());

                reviewsUI.add(reviewUI);
            }
            reviewsView.showReviews(reviewsUI);
        }


        @Override
        public void onError() {

        }

}
