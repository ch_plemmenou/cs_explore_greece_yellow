package com.yellow.exploregreece.features.login.data;

import android.content.Context;
import android.os.AsyncTask;

import com.yellow.exploregreece.base.ExploreGreeceDatabase;
import com.yellow.exploregreece.features.login.domain.LoginInteractor;
import com.yellow.exploregreece.features.login.domain.UserDomain;
import com.yellow.exploregreece.rest.RestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class LoginInteractorImpl implements LoginInteractor {


    @Override
    public void login(final OnLoginFinishListener listener, final Context ctx, final String username, final String password) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //Get user from db
                final UserDao dao = ExploreGreeceDatabase.getDatabase(ctx).userDao();
                UserDomain loggedInUser = dao.getLoggedInUser(password, username);
                Timber.d("");
                if (loggedInUser == null) {
                    // Get user from network
                    final UserDomain user = new UserDomain(username, password);
                    Call<UserDomain> call = RestClient.call().login(user);
                    call.enqueue(new Callback<UserDomain>() {
                        @Override
                        public void onResponse(Call<UserDomain> call, Response<UserDomain> response) {
                            if (response.code() == 200) {
                                //insert logged in user in local db
                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        user.setLoggedIn(true);
                                        dao.insertUser(user);
                                        listener.onSuccess();
                                    }
                                });
                            } else {
                                try {
                                    JSONObject error = null;
                                    error = new JSONObject(response.errorBody().string());
                                    listener.onError(error.getString("message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    listener.onError(null);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    listener.onError(null);
                                }

                            }

                        }

                        @Override
                        public void onFailure(Call<UserDomain> call, Throwable t) {
                            listener.onError(t.getMessage());
                        }
                    });
                } else {
                    listener.onSuccess();
                }
            }
        });
    }

}
