package com.yellow.exploregreece.features.tours.presentation;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.tours.domain.Tour;
import com.yellow.exploregreece.features.tours.domain.OnTourClickListener;

import java.util.ArrayList;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class TourRvAdapter extends RecyclerView.Adapter<TourRvAdapter.ToursViewHolder> {

    private OnTourClickListener listener;
    private ArrayList<TourUI> tours;


    public TourRvAdapter(ArrayList<TourUI> tours, OnTourClickListener listener) {
        this.tours = tours;
        this.listener = listener;
    }

    public static class ToursViewHolder extends RecyclerView.ViewHolder {
        TextView mTourPackageName;
        TextView mDescription;
        LinearLayout mTourItemRoot;


        public ToursViewHolder(View v) {
            super(v);
            mTourPackageName = v.findViewById(R.id.tour_name);
            mDescription = v.findViewById(R.id.tour_description);
            mTourItemRoot = v.findViewById(R.id.tour_item_root);

        }
    }

    @NonNull
    @Override
    public ToursViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_tour_screen, viewGroup, false);
        ToursViewHolder vh = new ToursViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ToursViewHolder viewHolder, int i) {
        final int position = i;
        final TourUI tour = tours.get(position);
        viewHolder.mTourPackageName.setText(tours.get(i).getTitle());
        viewHolder.mDescription.setText(tours.get(i).getDescription());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            viewHolder.mDescription.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        viewHolder.mTourItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTourClicked(tour);
            }
        });

    }

    @Override
    public int getItemCount() {
        return tours.size();
    }
}
