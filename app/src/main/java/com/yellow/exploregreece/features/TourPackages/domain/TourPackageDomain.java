package com.yellow.exploregreece.features.TourPackages.domain;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity (tableName = "tourPackage")
public class TourPackageDomain {

    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private double averageReviewScore;
    private String region;



    public TourPackageDomain(String id, String name, double averageReviewScore, String region) {
        this.id = id;
        this.name = name;
        this.averageReviewScore = averageReviewScore;
        this.region = region;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAverageReviewScore() {
        return averageReviewScore;
    }

    public void setAverageReviewScore(double averageReviewScore) {
        this.averageReviewScore = averageReviewScore;
    }

    public String getTourpackId() {
        return id;
    }

    public void setTourpackId(String tourpackId) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}

