package com.yellow.exploregreece.features.addReview.domain;

import android.content.Context;

public interface AddReviewPresenter {

    void sendReview(String tourpackageID, int score, String comment, Context ctx);

}
