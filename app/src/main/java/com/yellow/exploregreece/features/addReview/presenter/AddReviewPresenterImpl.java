package com.yellow.exploregreece.features.addReview.presenter;

import android.content.Context;

import com.yellow.exploregreece.features.addReview.domain.AddReviewInteractor;
import com.yellow.exploregreece.features.addReview.data.AddReviewInteractorImpl;
import com.yellow.exploregreece.features.addReview.domain.AddReviewPresenter;
import com.yellow.exploregreece.features.addReview.domain.AddReviewView;

public class AddReviewPresenterImpl implements AddReviewPresenter,AddReviewInteractor.OnSendReviewListener {

    AddReviewView addReviewView;
    AddReviewInteractor addReviewInteractor;

    public AddReviewPresenterImpl(AddReviewView addReviewView) {
        this.addReviewView = addReviewView;
        addReviewInteractor = new AddReviewInteractorImpl();
    }

    @Override
    public void sendReview(String tourpackageID, int score, String comment, Context ctx) {
        addReviewInteractor.sendReview(this, ctx, score,comment, tourpackageID);
    }

    @Override
    public void onSuccess() {
        addReviewView.reviewSent(true);
    }

    @Override
    public void onError() {
        addReviewView.reviewSent(false);
    }
}
