package com.yellow.exploregreece.features.tours.domain;

import android.content.Context;

import java.util.ArrayList;

public interface TourInteractor {

    void getTours(OnToursFinishListener listener,Context ctx, String tourpackage_id);
    //void getTours(OnToursFinishListener listener, String tourPackId);


    interface OnToursFinishListener {

        void onSuccess(ArrayList<Tour> tours);

        void onError();

    }
}
