package com.yellow.exploregreece.features.login.presentation;

import android.content.Context;

import com.yellow.exploregreece.features.login.domain.LoginView;
import com.yellow.exploregreece.features.login.data.LoginInteractorImpl;
import com.yellow.exploregreece.features.login.domain.LoginInteractor;
import com.yellow.exploregreece.features.login.domain.LoginPresenter;

public class LoginPresenterImpl implements LoginPresenter,LoginInteractor.OnLoginFinishListener {

    LoginView loginView;
    LoginInteractor interactor;


    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        interactor = new LoginInteractorImpl();
    }

    @Override
    public void checkCredentials(String username, String password, Context ctx) {
        interactor.login(this,ctx,username,password);
    }


    @Override
    public void onSuccess() {
        loginView.isLoggedIn(true,null);
    }

    @Override
    public void onError(String message) {
        loginView.isLoggedIn(false,message);
    }
}
