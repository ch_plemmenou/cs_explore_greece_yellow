package com.yellow.exploregreece.features.reviews.domain;

import android.content.Context;

import java.util.ArrayList;

public interface ReviewInteractor {

    void getReviews(OnReviewsFinishListener listener,Context ctx, String tourpackage_id, boolean refresh);

    interface OnReviewsFinishListener{

        void onSuccess(ArrayList<Review> reviews);

        void onError();
    }

}

