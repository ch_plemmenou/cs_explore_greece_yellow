package com.yellow.exploregreece.features.addReview.domain;

public interface AddReviewView {

    void reviewSent(boolean result);

}
