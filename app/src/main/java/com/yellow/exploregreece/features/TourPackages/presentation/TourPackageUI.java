package com.yellow.exploregreece.features.TourPackages.presentation;

import android.os.Parcel;
import android.os.Parcelable;

public class TourPackageUI implements Parcelable {

    private String tourpackId;
    private String name;
    private double rating;
    private int ratingColor;
    private int regionColor;
    private String regionName;


    public TourPackageUI(String name) {
        this.name = name;
    }

    public TourPackageUI(String tourpackId, String name, double rating, String regionName) {
        this.tourpackId = tourpackId;
        this.name = name;
        this.rating = rating;
        this.regionName = regionName;
    }

    public TourPackageUI(String tourpackId, String name, double rating, int ratingColor, int regionColor, String regionName) {
        this.tourpackId = tourpackId;
        this.name = name;
        this.rating = rating;
        this.ratingColor = ratingColor;
        this.regionColor = regionColor;
        this.regionName = regionName;
    }

    public String getTourpackId() {
        return tourpackId;
    }

    public void setTourpackId(String tourpackId) {
        this.tourpackId = tourpackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getRatingColor() {
        return ratingColor;
    }

    public void setRatingColor(int ratingColor) {
        this.ratingColor = ratingColor;
    }

    public int getRegionColor() {
        return regionColor;
    }

    public void setRegionColor(int regionColor) {
        this.regionColor = regionColor;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }


    protected TourPackageUI(Parcel in) {
        tourpackId = in.readString();
        name = in.readString();
        rating = in.readDouble();
        ratingColor = in.readInt();
        regionColor = in.readInt();
        regionName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tourpackId);
        dest.writeString(name);
        dest.writeDouble(rating);
        dest.writeInt(ratingColor);
        dest.writeInt(regionColor);
        dest.writeString(regionName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TourPackageUI> CREATOR = new Parcelable.Creator<TourPackageUI>() {
        @Override
        public TourPackageUI createFromParcel(Parcel in) {
            return new TourPackageUI(in);
        }

        @Override
        public TourPackageUI[] newArray(int size) {
            return new TourPackageUI[size];
        }
    };
}