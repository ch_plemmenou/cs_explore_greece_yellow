package com.yellow.exploregreece.features.addReview.domain;

import android.content.Context;

public interface AddReviewInteractor {

    void sendReview(OnSendReviewListener listener, Context ctx, int score, String comment, String tourpackageID);

    interface OnSendReviewListener{

        void onSuccess();

        void onError();

    }

}
