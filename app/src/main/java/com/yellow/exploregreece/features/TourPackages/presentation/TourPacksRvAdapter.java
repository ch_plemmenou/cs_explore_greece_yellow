package com.yellow.exploregreece.features.TourPackages.presentation;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.domain.OnTourPacksClickListener;

import java.util.ArrayList;


public class TourPacksRvAdapter extends RecyclerView.Adapter<TourPacksRvAdapter.TourPacksViewHolder> {

        private ArrayList<TourPackageUI> tourPackages;
        private OnTourPacksClickListener listener;
        private Context context;

        public TourPacksRvAdapter (ArrayList<TourPackageUI> tourPackages, OnTourPacksClickListener listener, Context context){
            this.tourPackages = tourPackages;
            this.listener = listener;
            this.context = context;
        }


        public static class TourPacksViewHolder extends RecyclerView.ViewHolder{
            TextView nPackName;
            TextView nRegionName;
            RatingBar nRatingBar;
            RelativeLayout nTourPackItemRoot;



            public TourPacksViewHolder(View v){
                super(v);
                nPackName = v.findViewById(R.id.tourpack_name);
                nRegionName = v.findViewById(R.id.tourpack_region);
                nRatingBar = v.findViewById(R.id.rating_bar);
                nTourPackItemRoot = v.findViewById(R.id.tour_pack_item_root);
            }

        }

        @NonNull
        @Override
        public TourPacksViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_tourpack_item,viewGroup,false);

            TourPacksViewHolder vh = new TourPacksViewHolder(v);

            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull TourPacksViewHolder tourPacksViewHolder, int i) {
            final int position = i;

            final TourPackageUI tourPackage = tourPackages.get(position);

            tourPacksViewHolder.nPackName.setText(tourPackage.getRegionColor());
            tourPacksViewHolder.nPackName.setText(tourPackage.getName());

            tourPacksViewHolder.nRegionName.setText(tourPackage.getRegionName());
            tourPacksViewHolder.nRegionName.setTextColor(context.getResources().getColor(tourPackage.getRegionColor()));

            tourPacksViewHolder.nRatingBar.setRating((int)tourPackage.getRating());
            DrawableCompat.setTint(tourPacksViewHolder.nRatingBar.getProgressDrawable(), context.getResources().getColor(tourPackage.getRatingColor()));
//            if (position == 1) {
//                LayerDrawable drawable = (LayerDrawable) tourPacksViewHolder.nRatingBar.getProgressDrawable();
//                drawable.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.pink), PorterDuff.Mode.SRC_ATOP);
//            }
//            else{
//                LayerDrawable drawable = (LayerDrawable) tourPacksViewHolder.nRatingBar.getProgressDrawable();
//                drawable.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
//            }
            tourPacksViewHolder.nTourPackItemRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onTourPacksClicked(tourPackage);

                }
            });
        }

        @Override
        public int getItemCount() {

            return tourPackages.size();
        }
    }

