package com.yellow.exploregreece.features.addReview.data;

public class ReviewDomain {

    int score;
    String comment;
    String username;

    public ReviewDomain(int score, String comment, String username) {
        this.score = score;
        this.comment = comment;
        this.username = username;
    }
}
