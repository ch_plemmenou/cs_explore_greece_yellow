package com.yellow.exploregreece.features.TourPackages.domain;

import android.content.Context;

import java.util.ArrayList;

public interface TourPacksInteractor {

    void getTourPacks(OnTourPacksFinishListener listener, Context ctx, boolean refresh);
    void getFilteredTourPacks(OnTourPacksFinishListener listener, String filterString);

    interface OnTourPacksFinishListener{
        void onSuccess (ArrayList<TourPackageDomain> tourPackages);
        void onError();

    }
}
