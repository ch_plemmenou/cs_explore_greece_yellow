package com.yellow.exploregreece.features.tours.domain;

import com.yellow.exploregreece.features.tours.presentation.TourUI;

import java.util.ArrayList;

public interface TourView {

    void showTours(ArrayList<TourUI> tours);
}
