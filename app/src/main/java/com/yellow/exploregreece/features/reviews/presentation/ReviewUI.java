package com.yellow.exploregreece.features.reviews.presentation;

public class ReviewUI  {

    private String reviewId;
    private String name;
    private String description;
    private int rating;




    public ReviewUI(String reviewId, String name, String description) {
        this.reviewId = reviewId;
        this.name = name;
        this.description = description;
    }

    public ReviewUI(String name, String description, int rating) {
        this.name = name;
        this.description = description;
        this.rating = rating;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
