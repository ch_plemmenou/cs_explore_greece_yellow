package com.yellow.exploregreece.features.login.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.yellow.exploregreece.R;

public class LoginActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.login_root, new LoginFragment())
                .commit();


    }
}
