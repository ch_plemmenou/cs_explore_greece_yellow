package com.yellow.exploregreece.features.reviews.domain;

import com.yellow.exploregreece.features.reviews.presentation.ReviewUI;

import java.util.ArrayList;

public interface ReviewsView {

    void showReviews(ArrayList<ReviewUI> reviews);

}


