package com.yellow.exploregreece.features.tours.domain;

import com.yellow.exploregreece.features.tours.presentation.TourUI;

public interface OnTourClickListener {

    void onTourClicked(TourUI tour);
}
