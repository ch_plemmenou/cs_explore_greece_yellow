package com.yellow.exploregreece.features.TourPackages.presentation;

import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.presentation.TourPacksFragment;


public class TourPacksActivity extends AppCompatActivity {


            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_tour_packs);
                Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
                setSupportActionBar(toolbar);

                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.tour_packages_root, new TourPacksFragment())
                        .commit();
            }
        }