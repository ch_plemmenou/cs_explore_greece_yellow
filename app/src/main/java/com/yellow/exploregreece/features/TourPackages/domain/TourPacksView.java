package com.yellow.exploregreece.features.TourPackages.domain;

import com.yellow.exploregreece.features.TourPackages.presentation.TourPackageUI;

import java.util.ArrayList;

public interface TourPacksView {

    void showTourPacks(ArrayList<TourPackageUI> tourPackages);

    void showGeneralError();
}
