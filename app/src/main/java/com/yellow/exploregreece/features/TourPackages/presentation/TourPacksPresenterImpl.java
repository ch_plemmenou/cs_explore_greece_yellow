package com.yellow.exploregreece.features.TourPackages.presentation;

import android.content.Context;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.domain.RegionName;
import com.yellow.exploregreece.features.TourPackages.data.TourPacksInteractorImpl;
import com.yellow.exploregreece.features.TourPackages.domain.TourPackageDomain;
import com.yellow.exploregreece.features.TourPackages.domain.TourPacksInteractor;
import com.yellow.exploregreece.features.TourPackages.domain.TourPacksPresenter;
import com.yellow.exploregreece.features.TourPackages.domain.TourPacksView;

import java.util.ArrayList;

public class TourPacksPresenterImpl implements TourPacksPresenter,TourPacksInteractor.OnTourPacksFinishListener {


    TourPacksView tourPacksView;
    TourPacksInteractor interactor;

    public TourPacksPresenterImpl(TourPacksView tourPacksView) {
        this.tourPacksView = tourPacksView;
        interactor = new TourPacksInteractorImpl();
    }


    @Override
    public void getTourPacks(Context ctx, boolean refresh) {
        interactor.getTourPacks(this, ctx, refresh);
    }


    @Override
    public void getFilteredTourPacks(String filterString) {
        interactor.getFilteredTourPacks(this, filterString);

    }

    @Override
    public void onSuccess(ArrayList<TourPackageDomain> tourPackages) {
        ArrayList<TourPackageUI> tourPackagesUI = new ArrayList<>();
        if (tourPackages != null && !tourPackages.isEmpty()) {
            for (TourPackageDomain tourPackage : tourPackages) {
                TourPackageUI tourPackageUI = new TourPackageUI(
                        tourPackage.getTourpackId(),
                        tourPackage.getName(),
                        tourPackage.getAverageReviewScore(),
                        tourPackage.getRegion()
                );

                setColorBasedOnRegion(tourPackage, tourPackageUI);

                setColorBaseOnRating(tourPackageUI);

                tourPackagesUI.add(tourPackageUI);
            }
            tourPacksView.showTourPacks(tourPackagesUI);
        }
    }

    private void setColorBaseOnRating(TourPackageUI tourPackageUI) {
        if (tourPackageUI.getRating() >= 4.0) {
            tourPackageUI.setRatingColor(R.color.green);
        }else if (tourPackageUI.getRating() >= 3)
            tourPackageUI.setRatingColor(R.color.yellow);
        else
            tourPackageUI.setRatingColor(R.color.red);
    }




    private void setColorBasedOnRegion(TourPackageDomain tourPackage, TourPackageUI tourPackageUI) {
        if(tourPackage.getRegion().equals(RegionName.CRETE.getString()))
            tourPackageUI.setRegionColor(R.color.Purple);
        else if(tourPackage.getRegion().equals(RegionName.PELOPONNESE.getString()))
            tourPackageUI.setRegionColor(R.color.pink);
        else if (tourPackage.getRegion().equals(RegionName.MACEDONIA.getString()))
            tourPackageUI.setRegionColor(R.color.colorPrimaryDark);
        else if (tourPackage.getRegion().equals(RegionName.THESSALY.getString()))
            tourPackageUI.setRegionColor(R.color.dark_grey);
        else if (tourPackage.getRegion().equals(RegionName.THRACE.getString()))
            tourPackageUI.setRegionColor(R.color.Blue);
        else if(tourPackage.getRegion().equals(RegionName.AEAGEAN.getString()))
            tourPackageUI.setRegionColor(R.color.Orange);
        else if (tourPackage.getRegion().equals(RegionName.IONIAN.getString()))
            tourPackageUI.setRegionColor(R.color.yellow);
        else if (tourPackage.getRegion().equals(RegionName.STEREAHELLAS.getString()))
            tourPackageUI.setRegionColor(R.color.green);
    }

    @Override
    public void onError() {
        tourPacksView.showGeneralError();
    }
}
