package com.yellow.exploregreece.features.tours.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.yellow.exploregreece.R;

public class TourActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.tour_root, new TourFragment())
                .commit();
    }
}
