package com.yellow.exploregreece.features.reviews.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.yellow.exploregreece.R;
import com.yellow.exploregreece.features.TourPackages.presentation.TourPackageUI;
import com.yellow.exploregreece.features.reviews.domain.ReviewsPresenter;
import com.yellow.exploregreece.features.reviews.domain.ReviewsView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewFragment extends Fragment implements ReviewsView {


    @BindView(R.id.reviews_recycler_view)
    RecyclerView reviewsRv;

    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;

    @BindView(R.id.review_tour_package_name)
    TextView myTourPackageName;

    @BindView(R.id.review_tour_package_rating)
    RatingBar myTourPackageRating;

    @BindView(R.id.swipe_refresh_review)
    SwipeRefreshLayout mySwipeRefresLayout;

    private ReviewsPresenter presenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Intent intent = getActivity().getIntent();
        final TourPackageUI tourpackage = intent.getExtras().getParcelable("tourpackage");

        View v = inflater.inflate(R.layout.fragment_review, container, false);
        ButterKnife.bind(this, v);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(myToolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);

        mySwipeRefresLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getReviews(getActivity(),tourpackage.getTourpackId(),true);
            }
        });

        myTourPackageName.setText(tourpackage.getName());
        myTourPackageRating.setRating(Float.valueOf(String.valueOf(tourpackage.getRating())));

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                reviewsRv.setLayoutManager(layoutManager);
                presenter = new ReviewsPresenterImpl(this);
                presenter.getReviews(getActivity(),tourpackage.getTourpackId(),false);

        return v;
    }

    @Override
    public void showReviews(ArrayList<ReviewUI> reviews) {

        mySwipeRefresLayout.setRefreshing(false);

        ReviewRvAdapter reviewsRvAdapter = new ReviewRvAdapter(reviews,getActivity());
        reviewsRv.setAdapter(reviewsRvAdapter);

    }
}