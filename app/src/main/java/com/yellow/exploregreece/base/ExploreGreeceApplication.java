package com.yellow.exploregreece.base;

import android.app.Application;

import timber.log.Timber;

public class ExploreGreeceApplication extends Application {


    public ExploreGreeceApplication() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
