package com.yellow.exploregreece.base;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.yellow.exploregreece.features.TourPackages.data.TourPackagesDao;
import com.yellow.exploregreece.features.TourPackages.domain.TourPackageDomain;
import com.yellow.exploregreece.features.login.data.UserDao;
import com.yellow.exploregreece.features.login.domain.UserDomain;
import com.yellow.exploregreece.features.reviews.domain.Review;
import com.yellow.exploregreece.features.reviews.data.ReviewDao;
import com.yellow.exploregreece.features.tours.domain.Tour;
import com.yellow.exploregreece.features.tours.data.TourDao;

@Database(entities = {Tour.class,UserDomain.class,TourPackageDomain.class, Review.class}, version = 1, exportSchema = false)
abstract public class ExploreGreeceDatabase extends RoomDatabase {
    public abstract TourDao toursDao();
    public abstract UserDao userDao();
    public abstract TourPackagesDao tourPackagesDao();
    public abstract ReviewDao reviewsDao();

    static private ExploreGreeceDatabase INSTANCE;

    public static ExploreGreeceDatabase getDatabase(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(ctx.getApplicationContext(),
                    ExploreGreeceDatabase.class, "explore_greece_databse")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }


}


