package com.yellow.exploregreece.rest.responses;

import com.yellow.exploregreece.features.tours.domain.Tour;

import java.util.ArrayList;

public class ToursResponseItem {

        private ArrayList<Tour> Items;

        public ArrayList<Tour> getItems() {
            return Items;
        }

        public void setItems(ArrayList<Tour> items) {
            Items = items;
        }
}
