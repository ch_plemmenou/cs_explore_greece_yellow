package com.yellow.exploregreece.rest;

import com.yellow.exploregreece.features.addReview.data.ReviewDomain;
import com.yellow.exploregreece.features.TourPackages.domain.TourPackageDomain;
import com.yellow.exploregreece.features.login.domain.UserDomain;
import com.yellow.exploregreece.features.reviews.domain.Review;
import com.yellow.exploregreece.features.tours.domain.Tour;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface RestAPI {

    @GET("tourPackages")
    Call<ArrayList<TourPackageDomain>> fetchTourPackages();

    @GET("tourPackages/{tourpackage_id}/tours")
    Call<ArrayList<Tour>> fetchTours(@Path("tourpackage_id") String packageID);

    @GET("tourPackages/{tourpackage_id}/reviews")
    Call<ArrayList<Review>> fetchReviews(@Path("tourpackage_id") String packageID);

    //    @Headers("Content-Type: application/json")
    @POST("login")
    Call<UserDomain> login(@Body UserDomain loginObject);

    @POST("tourPackages/{id}/reviews")
    Call<Void> sendReview(@Path("id") String packageID, @Body ReviewDomain reviewObject);


}
