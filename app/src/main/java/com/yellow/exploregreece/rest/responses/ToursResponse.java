package com.yellow.exploregreece.rest.responses;

public class ToursResponse {
     private ToursResponseItem Result;

    public ToursResponseItem getResult() {
        return Result;
    }

    public void setResult(ToursResponseItem result) {
        Result = result;
    }
}